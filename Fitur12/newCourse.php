<html>
  <head>
    <meta charset="utf-8">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
</head>
  <body>
    <?php
      include "functs.php";
      connectdb();
      include "navbar.php";
    ?>
    </br>
    </br>
    </br>
    <div class = "container">
      <?php
        if(isset($_SESSION["error"])){
          echo   '<div class="alert alert-danger">'.$_SESSION["error"].'</div>';
        }
        unset($_SESSION["error"]);
      ?>
      <form action="prosesNewCourse.php" method="post">
        <div class="form-group">
          <label>Course Name</label>
          <input type="text" class="form-control" placeholder="Nama Kursus" name="course_name" required value=<?php if(isset($_SESSION["course_name"])){echo '"'.$_SESSION["course_name"].'"';} ?>>
          <small class="form-text text-muted">Nama Kursus 1-100 huruf</small>
        </div>
        <div class="form-group">
          <label>Jumlah Maksimal Murid</label>
          <input type="number" class="form-control" placeholder="Jumlah Maksimal Murid" name="maks" required value=<?php if(isset($_SESSION["maks"])){echo '"'.$_SESSION["maks"].'"';} ?>>
        </div>
        <div class="form-group">
          <label>Awal pendaftaran</label>
          <input type="date" class="form-control" name="awal_daftar" value=<?php if(isset($_SESSION["awal_daftar"])){echo '"'.$_SESSION["awal_daftar"].'"';} ?> required>
        </div>
        <div class="form-group">
          <label>Akhir pendaftaran</label>
          <input type="date" class="form-control" name="akhir_daftar" value=<?php if(isset($_SESSION["akhir_daftar"])){echo '"'.$_SESSION["akhir_daftar"].'"';} ?> required>
        </div>
        <div class="form-group">
          <label>Awal kelas</label>
          <input type="date" class="form-control" name="awal_kelas" value=<?php if(isset($_SESSION["awal_kelas"])){echo '"'.$_SESSION["awal_kelas"].'"';} ?> required>
        </div>
        <div class="form-group">
          <label>Akhir kelas</label>
          <input type="date" class="form-control" name="akhir_kelas" value=<?php if(isset($_SESSION["akhir_kelas"])){echo '"'.$_SESSION["akhir_kelas"].'"';} ?> required>
        </div>
        <div class="form-group">
          <label>Kategori course</label>
          <select  class="form-control" name="kategori">
              <?php
              $query = "SELECT nama_kategori FROM KATEGORI";
              $result = pg_query($query);
              while ($row = pg_fetch_row($result)) {
                echo '<option>'.$row[0].'</option>';
              }
              ?>
           </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
  </body>
</html>