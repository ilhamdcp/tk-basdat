<?php
	include "functs.php";
	include "navbar.php";

	$string = $_GET['id'];
	$arr = (explode("/",$string));
	$posisi_id = substr($arr[0],0,1);
	$lowongan_id = $arr[1];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>SILOKER</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="content">
			<?php 
				$query = "select nama, username, no_ktp, nama_jalan, no_hp, cv from pengguna_userumum where username in (select username from pelamaran where lowongan_id='$lowongan_id' and posisi_id='$posisi_id');";
				$result = pg_query($query);
			?>

			<?php
				if($_SERVER["REQUEST_METHOD"] == "POST"){
					$username = $_POST["username"];

					$query = "update pelamaran set status = 2 where lowongan_id='$lowongan_id' and posisi_id='$posisi_id';";
					pg_query($query);
					// Nambah jumlah terima saat ini
					$query = "select jml_penerima_saatini from posisi where posisi_id='$posisi_id' and lowongan_id = '$lowongan_id';";
					$row = pg_fetch_row(pg_query($query));
					$now = $row[0] + 1;
					$query = "update posisi set jml_penerima_saatini = $now where lowongan_id = '$lowongan_id' and lowongan_id='$lowongan_id'";
					pg_query($query);
				}
			?>
			<table class="table table-bordered">
				<tr>
					<th> Pelamar </th>
				</tr>
					<?php
						while($row = pg_fetch_row($result)){
							$nama = $row[0];
							$username = $row[1];
							$no_ktp = $row[2];
							$nama_jalan = $row[3];
							$no_hp = $row[4];
							$cv = $row[5];
							$collaps = '
							<a href="#detil" data-toggle="collapse" class="btn btn-primary pull-right"> > </a>
							<div id="detil" class="collapse">
							  	<table class="table table-bordered">
							  		<tr>
							  			<td>Username: '.$username.'</td>
							  		</tr>
							  		<tr>
							  			<td>No. KTP: '.$no_ktp.'</td>
							  		</tr>
							  		<tr>
							  			<td>Nama Jalan: '.$nama_jalan.'</td>
							  		</tr>
							  		<tr>
							  			<td>Nomor Handphone:'.$no_hp.'</td>
							  		</tr>
							  		<tr>
							  			<td>CV: '.$cv.'</td>
							  		</tr>
							  	</table>
							</div>
							';

							$query = "select status from pelamaran where lowongan_id='$lowongan_id' and posisi_id='$posisi_id' and username='$username'";
							$resultStatus = pg_fetch_row(pg_query($query));
							$status = $resultStatus[0];
							if($status > 1){
								$button = '
								<div id="lamar">
									<form method="post">
										<input type="hidden" name="nama" value="'.$nama.'">
										<input type="hidden" name="username" value="'.$username.'">
										<input type="hidden" name="no_ktp" value="'.$no_ktp.'">
										<input type="hidden" name="nama_jalan" value="'.$nama_jalan.'">
										<input type="hidden" name="no_hp" value="'.$no_hp.'">
										<input type="hidden" name="cv" value="'.$cv.'">
										<input type="submit" name="button-terima" id="button-terima" class="btn pull-right" value="Diterima" disabled>
									</form>
								</div>
								';
							}
							else{
								$button = '
								<div id="lamar">
									<form method="post">
										<input type="hidden" name="nama" value="'.$nama.'">
										<input type="hidden" name="username" value="'.$username.'">
										<input type="hidden" name="no_ktp" value="'.$no_ktp.'">
										<input type="hidden" name="nama_jalan" value="'.$nama_jalan.'">
										<input type="hidden" name="no_hp" value="'.$no_hp.'">
										<input type="hidden" name="cv" value="'.$cv.'">
										<input type="submit" name="button-terima" id="button-terima" class="btn btn-success pull-right" value="Terima">
									</form>
								</div>
								';
							}

							
							echo '<tr><td>'.$nama.$collaps.$button.'</td></tr>';
						}
					?>
			</table>
		</div>
	</div>
</body>
</html>