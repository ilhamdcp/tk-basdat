<html>
	<head>
	<meta charset="UTF-8">
	<title>SILOKER</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
	
	<body>
		<?php
		include "functs.php";
  		include "navbar.php";
  		?>
  		<?php
  			displayCompanyProfile();
  			echo "<br>";
  		?>

  		<!-- Tambah Lowongan -->
	  	<div class="container pull-right">
	  	  <!-- Button to Open the Modal -->
	  	  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalLowongan">
	  	    Tambah Lowongan
	  	  </button>
	  	  <!-- Modals -->
	  	  <div class="modal fade" id="modalLowongan">
	  	  	<div class="modal-dialog">
	  		    <!-- Modal Content  -->
	  			<div class="modal-content">
	  		      <!-- Modal Header -->
	  		      <div class="modal-header">
	  		        <button type="button" class="close" data-dismiss="modal">&times;</button>
	  		      </div>
	  		      
	  		      <!-- Modal body -->
	  		      <div class="modal-body">
	  		      	<!-- Form -->
	  		        <form name="formLowongan" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" onsubmit="return validateForm()" method="POST">
	  		        	<div class="form-group">
	  		        		<label for="namaLowongan">Nama Lowongan:</label>
	  		        		<input class="form-control" type="text" name="namaLowongan">
	  		        	</div>
	  		        	<div class="form-group">
	  		        		<label for="tanggalBuka">Tanggal Buka:</label>
	  		        		<input class="form-control" type="date" name="tanggalBuka">
	  		        	</div>
	  		        	<div class="form-group">
	  		        		<label for="tanggalTutup">Tanggal Tutup:</label>
	  		        		<input class="form-control" type="date" name="tanggalTutup">
	  		        	</div>
	  		        	
	  		        	<button type="submit" class="btn btn-primary">Submit</button>
	  		        </form>
	  		      </div>
	  		    </div>
	  		  </div>
	  		</div>
	  		<script>
	  			function validateForm(){
	  				var tanggalTutup = document.forms["formLowongan"]["tanggalTutup"].value;
	  				var tanggalBuka = document.forms["formLowongan"]["tanggalBuka"].value;
	  				if(tanggalTutup < tanggalBuka){
	  					alert("penanggalan salah");
	  					return false;
	  				}
	  			}
	  		</script>
	  	</div>

  		<?php
  			tambahLowongan();
  			displayApplications();
  		?>
	</body>
</html>