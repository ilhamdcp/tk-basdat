<html>
	<head>
		<meta charset="UTF-8">
		<title>SILOKER</title>
		<script type="text/javascript" src="src/js/jquery-3.1.1.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	</head>
	
	<body>
		<?php
		include "functs.php";
  		include "navbar.php";
  		?>
  		<?php
  			displayProfile();
  			echo "<br>";
  			displayApplicationHistory();
  		?>
	</body>
</html>