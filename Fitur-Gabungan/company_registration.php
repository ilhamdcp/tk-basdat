<?php
include 'functs.php';
if(!isset($_SESSION["username"])) {
    header("Location: index.php");
    exit();
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        li {
            list-style: none;
        }
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<?php
include 'navbar.php';
?>
<h2>Company Registration</h2>
<hr/>
<div id="message"></div>
<div style="width:500px; margin: auto; padding: 10px">
    <form name="insert" id="company_form" action="admin_registration.php" method="POST">
        <div class="form-group">
            <label class="col-form-label" for="No_akta">No Akta</label>
            <input type="text" id="No_akta" name="No_akta" class="form-control"/>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="Namacompany">Nama Company</label>
            <input type="text" id="Namacompany" name="Namacompany" class="form-control"/>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="No_telp">No Telp</label>
            <input type="text" id="No_telp" name="No_telp" class="form-control"/>
            <div id="No_telp-warning"></div>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="Nama_jalan">Nama Jalan</label>
            <input type="text" id="Nama_jalan" name="Nama_jalan" class="form-control"/>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="Provinsi">Provinsi</label>
            <input type="text" id="Provinsi" name="Provinsi" class="form-control"/>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="Kota">Kota</label>
            <input type="text" id="Kota" name="Kota" class="form-control"/>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="Kodepos">Kode Pos</label>
            <input type="text" id="Kodepos" name="Kodepos" class="form-control"/>
            <div id="postal-warning"></div>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="Deskripsi">Deskripsi</label>
            <input type="text" id="Deskripsi" name="Deskripsi" class="form-control"/>
        </div>
        <input type="submit" name='submit-company' id="submit" class="btn btn-right btn-success"/>
    </form>
</div>
<script>
    $(document).ready(function () {
        $("#company_form").submit(function () {
            if ($.trim($("#No_akta").val()) === '' || !$.isNumeric($("#No_telp").val()) || $.trim($("#Namacompany").val()) === '' ||
                $.trim($("#Nama_jalan").val()) === '' || $.trim($("#Provinsi").val()) === '' || $.trim($("#Kota").val()) === '' ||
                $.trim($("#Kodepos").val()) === '') {
                alert("Field yang boleh kosong hanya deksripsi");
                return false;
            }
            else return true;
        });

        $("#No_telp").on('keyup', function () {
            var notelp = $("#No_telp").val();
            if (!$.isNumeric(notelp) && notelp.length > 0) {
                $("#No_telp-warning").html("No telp hanya berisi angka");
                $("#No_telp-warning").removeClass().addClass("alert alert-danger");
            }
            else {
                $("#No_telp-warning").html("");
                $("#No_telp-warning").removeClass();
            }
        })
    });

</script>
</body>
</html>
