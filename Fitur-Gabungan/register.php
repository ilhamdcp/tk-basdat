<?php
include 'functs.php';
if(isset($_SESSION["username"])) {
	header("Location: index.php");
	exit();

}

if(!isset($_SESSION["registererr"]))
{
	$_SESSION["usererr"] = null;
	$_SESSION["passerr"] = null;
	$_SESSION["iderr"] = null;
	$_SESSION["phonerr"] = null;
	$_SESSION["postalnerr"] = null;
}

?>

<html>
<head>
	<meta charset="UTF-8">
	<title>SILOKER</title>
	<script type="text/javascript" src="./scripts/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="./scripts/validateform.js"></script>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script>
		$( function() {
			$( "#dateofbirth" ).datepicker();
		} );
	</script>
</head>

<body>
	<?php
	include "navbar.php";
	?>
	<h1>Form Registrasi User</h1>

	<form action="./functs.php" method="post" name="btn-register">

		<div class="col-12 col-md-9">
			<div class="form-group row">
				<label for="reg-user" class="col-2 col-form-label">Username</label>
				<div class="col-10">
					<input name="reg-user" class="form-control required" type="text" value="" id="reg-user" required>
				</div>
			</div>
			<?php
			if(isset($_SESSION["usererr"])){
				?>
				<div class="alert alert-warning alert-dismissable">
				<?php echo $_SESSION["usererr"]; 
				unset($_SESSION["usererr"]);?>
				</div>
				<?php
			}?>

			<div class="form-group row">
				<label for="reg-pass" class="col-2 col-form-label">Password</label>
				<div class="col-10">
					<input name="reg-pass" class="form-control required" type="password" value="" id="reg-pass" required>
				</div>
			</div>
			<?php
			if(isset($_SESSION["passerr"])){
				?>
				<div class="alert alert-warning alert-dismissable">
				<?php echo $_SESSION["passerr"];
				 unset($_SESSION["passerr"]);?>
				</div>
				<?php
			}?>

			<div class="form-group row">
				<label for="fullname" class="col-2 col-form-label">Nama Lengkap</label>
				<div class="col-10">
					<input name="reg-fullname" class="form-control required" type="text" value="" id="reg-fullname" required>
				</div>
			</div>


			<div class="form-group row">
				<label for="idnumber" class="col-2 col-form-label">Nomor KTP</label>
				<div class="col-10">
					<input name="reg-idnumber" class="form-control required" type="text" value="" id="reg-idnumber" required>
				</div>
			</div>
			<?php
			if(isset($_SESSION["iderr"])){
				?>
				<div class="alert alert-warning alert-dismissable">
				<?php echo $_SESSION["iderr"]; 
				unset($_SESSION["iderr"]);?>
				</div>
				<?php
			}?>

			<div class="form-group row">
				<label for="idnumber" class="col-2 col-form-label">Nomor HP</label>
				<div class="col-10">
					<input name="reg-phone_number" class="form-control required" type="text" value="" id="reg-phone_number" required>
				</div>
			</div>
			<?php
			if(isset($_SESSION["phoneerr"])){
				?>
				<div class="alert alert-warning alert-dismissable">
				<?php echo $_SESSION["phoneerr"];
				unset($_SESSION["phoneerr"]); ?>
				</div>
				<?php
			}?>
			<div class="form-group row">
				<label for="dateofbirth" >Tanggal Lahir</label>
				<div class="col-10">
					<input id="reg-dateofbirth" name="reg-dateofbirth" type="text"  class="form-control required" required>
				</div>
			</div>
			<div class="form-group row">
				<label for="fullname" class="col-2 col-form-label">Jalan</label>
				<div class="col-10">
					<input name="reg-road" class="form-control required" type="text" value="" id="reg-road" required>
				</div>
			</div>

			<div class="form-group row">
				<label for="fullname" class="col-2 col-form-label">Kota</label>
				<div class="col-10">
					<input name="reg-city" class="form-control required" type="text" value="" id="reg-city" required>
				</div>
			</div>

			<div class="form-group row">
				<label for="fullname" class="col-2 col-form-label">Provinsi</label>
				<div class="col-10">
					<input name="reg-province" class="form-control required" type="text" value="" id="reg-province" required>
				</div>
			</div>

			<div class="form-group row">
				<label for="fullname" class="col-2 col-form-label">Kode Pos</label>
				<div class="col-10">
					<input name="reg-postal" class="form-control required" type="text" value="" id="reg-postal" required>
				</div>
			</div>
			<?php
			if(isset($_SESSION["postalerr"])){
				?>
				<div class="alert alert-warning alert-dismissable">
				<?php echo $_SESSION["postalerr"];
				unset($_SESSION["postalerr"]); ?>
				</div>
				<?php
			}?>
			<div class="form-group row">
				<label for="fullname" class="col-2 col-form-label">CV</label>
				<div class="col-10">
					<input name="reg-cv" class="form-control required" type="text" value="" id="reg-cv" required>
				</div>
			</div>

			<div class="row">
				<input type="submit" value="Register" name="btn-register" class='btn btn-default'>
			</div>
		</div>
		<br>
	</form>
</body>
</html>