<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">SILOKER</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="index.php">Index</a></li>
      <li><a href="lowonganAll.php">Lowongan</a></li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Online Course
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="online-course.php">Daftar Online Course</a></li>
          <li><a href="myonlinecourse.php">My Online Course</a></li>
        </ul>
      </li>
      <?php
      if(isset($_SESSION["username"])){
        ?>
        <li><a href="logout.php">Logout</a></li>
        <?php
      } else {
        ?> <li><a href="login.php">Login</a></li>
        <li><a href="register.php">Register</a></li>
        <?php
      }
      ?>

      <?php
      if(isset($_SESSION["role"])){
        if($_SESSION["role"] === "user") { 
          ?>  <li><a href="profile.php">Profile</a></li>
          <li><a href="all_company.php">Daftar Company</a></li>
          <li><a href="company_registration.php">Pendaftaran Company</a></li>
          <?php 
        } else if ($_SESSION["role"] === "admin" && $_SESSION["company"] === "t") {
          ?><li><a href="comprofile.php">Profile Company</a></li>
          <li><a href="all_company.php">Daftar Company</a></li>
          <?php
        } else if ($_SESSION["role"] === "admin" && $_SESSION["company"] === 'f') {
          ?><li><a href="all_company.php">Daftar Company</a></li>
          <?php
        }
      } 
      ?>
    </ul>
  </div>
</nav>